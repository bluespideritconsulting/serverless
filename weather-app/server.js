const express = require('express');
const bodyParser = require('body-parser');
const request = require('request');
const server = express()

const apiKey = 'fdbec703810db4f4b05fdee6ffd182bf';

server.use(express.static('public'));
server.use(bodyParser.urlencoded({ extended: true }));
server.set('view engine', 'ejs')

server.get('/', function (req, res) {
  res.render('index', {weather: null, error: null});
})

server.post('/', function (req, res) {
  let city = req.body.city;
  let url = `http://api.openweathermap.org/data/2.5/weather?q=${city}&units=metric&appid=${apiKey}`

  request(url, function (err, response, body) {
    if(err){
      res.render('index', {weather: null, error: 'Error, please try again'});
    } else {
      let weather = JSON.parse(body)
      if(weather.main == undefined){
        res.render('index', {weather: null, error: 'Error, please try again'});
      } else {
        let weatherText = `It's ${weather.main.temp} degrees in ${weather.name} and is currently ${weather.clouds.all}% Cloudy!`;
        res.render('index', {weather: weatherText, error: null});
      }
    }
  });
})

module.exports = server